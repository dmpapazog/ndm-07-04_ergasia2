#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "GL/freeglut.h"
#include "callbacks/callbacks.h"


void rectangle(void);
void cube(void);
void move(void);
void rescale(void);

GLuint draw;
GLdouble const dtheta = 2;
GLfloat scaleFactor = 1;
GLfloat inc = 0.01;
GLfloat rotCenter = -b;
GLfloat cubCenter = -b;


void rectangle(void)
{
  glBegin(GL_POLYGON);
    glVertex3i(-1,  1, 1);
    glVertex3i(-1, -1, 1);
    glVertex3i( 1, -1, 1);
    glVertex3i( 1,  1, 1);
  glEnd();
}

void initList()
{
  draw = glGenLists(6);
  glNewList(draw    , GL_COMPILE);
    // red near
    glColor3ub(255, 0, 0);
    rectangle();
  glEndList();

  glNewList(draw + 1, GL_COMPILE);
    glPushMatrix();
      // green far
      glColor3ub(0, 255, 0);
      glTranslatef(0, 0, -2);
      rectangle();
    glPopMatrix();
  glEndList();

  glNewList(draw + 2, GL_COMPILE);
    glPushMatrix();
      // blue right
      glColor3ub(0, 0, 255);
      glRotatef(90, 0, 1, 0);
      rectangle();
    glPopMatrix();
  glEndList();

  glNewList(draw + 3, GL_COMPILE);
    glPushMatrix();
      // orange left
      glColor3ub(255, 140, 0);
      glRotatef(-90, 0, 1, 0);
      rectangle();
    glPopMatrix();
  glEndList();

  glNewList(draw + 4, GL_COMPILE);
    glPushMatrix();
      // purple bottom
      glColor3ub(128, 0, 128);
      glRotatef(90, 1, 0, 0);
      rectangle();
    glPopMatrix();
  glEndList();

  glNewList(draw + 5, GL_COMPILE);
    glPushMatrix();
      // yellow top
      glColor3ub(255, 255, 0);
      glRotatef(-90, 1, 0, 0);
      rectangle();
    glPopMatrix();
  glEndList();
}

void cube(void)
{
  for (int i = 0; i < 6; ++i) {
    glCallList(draw + i);
  }
}

void display(void)
{
  glPushMatrix();
    glTranslatef(0, 0, cubCenter);
    glScalef(a / 2, a / 2, a / 2);
    cube();
  glPopMatrix();
  glutSwapBuffers();
}

void idle(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  move();
  glutPostRedisplay();
}

void move(void)
{
  glTranslatef(0, 0,  rotCenter);
  glRotatef(dtheta, vec[0], vec[1], vec[2]);
  glTranslatef(0, 0, -rotCenter);

  glTranslatef(0, 0,  cubCenter);
  rescale();
  glTranslatef(0, 0, -cubCenter);
}

void rescale(void)
{
  GLfloat oldScaleFactor = scaleFactor;

  if (scaleFactor > 3 || scaleFactor < 1) {
    inc *= -1;
  }
  scaleFactor += inc;
  glScalef(scaleFactor / oldScaleFactor,
					 scaleFactor / oldScaleFactor,
					 scaleFactor / oldScaleFactor);
}

void menu(int id)
{
	glLoadIdentity();
	scaleFactor = 1;

	switch (id) {
		case 1:
			rotCenter = -b;
      cubCenter = -b;
			break;
		case 2:
			rotCenter = -8 * b / 10;
			cubCenter = rotCenter - 10;
			break;
		case 3:
			exit(0);
		default:
			break;
	}

	glutPostRedisplay();
}