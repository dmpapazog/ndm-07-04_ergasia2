add_library(callbacks SHARED)
target_sources(callbacks PRIVATE
  callbacks.c
)
target_include_directories(callbacks PUBLIC
  ${PROJECT_SOURCE_DIR}/include
)
target_link_libraries(callbacks PUBLIC
  FreeGLUT::freeglut
  OpenGL::GLU
)
