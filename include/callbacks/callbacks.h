#ifndef CALLBACKS_H
#define CALLBACKS_H

#include "GL/freeglut.h"


typedef GLfloat Vector[3];

void display(void);
void initList(void);
void idle(void);
void menu(int id);

GLfloat const a = 8;
GLfloat const b = 90;
Vector 	const vec = {1, 2, 6};

#endif // CALLBACKS_H