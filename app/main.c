#include <stdio.h>
#include <string.h>
#include "GL/freeglut.h"
#include "callbacks/callbacks.h"

void init(void);
void initWindow(void);
void initMenu(void);
void setCallbacks(void);

int main(int argc, char **argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

  initWindow();
  glEnable(GL_DEPTH_TEST);
  initMenu();

  init();
  initList();

  setCallbacks();
  glutMainLoop();
  return 0;
}

void init(void)
{
  glClearColor(0, 0, 0, 0);

  glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(-15, 15, -15, 15, -b - 100, b + 100);
    // glTranslatef(0, 0, 5);

  glMatrixMode(GL_MODELVIEW);
}

void initWindow(void)
{
  glutInitWindowSize(500, 500);
  glutInitWindowPosition(100, 50);
  glutCreateWindow("Testing");
}

void setCallbacks(void)
{
  glutDisplayFunc(&display);
  glutIdleFunc(&idle);
}

void initMenu(void)
{
  glutCreateMenu(&menu);
    glutAddMenuEntry("Rotate at the (0, 0, 0).", 1);
    glutAddMenuEntry("Rotate at the (0, 0, -8*b/10).", 2);
    glutAddMenuEntry("Exit", 3);
  glutAttachMenu(GLUT_RIGHT_BUTTON);
}